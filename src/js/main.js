$(document).ready(function () {
// init liquidimg script
  $(".imgLiquidFill").imgLiquid({
    fill: true,
    horizontalAlign: "center",
    verticalAlign: "center"
  });
  $(".imgLiquidNoFill").imgLiquid({
    fill: false,
    horizontalAlign: "center",
    verticalAlign: "center"
  });
// btn to scroll
  $(function () {
    $(window).scroll(function () {
      if ($(this).scrollTop() != 0) {
        $('#toTop').fadeIn();
      } else {
        $('#toTop').fadeOut();
      }
    });
    $('#toTop').click(function () {
      $('body,html').animate({scrollTop: 0}, 400);
    });
    $('.header .nav-link').on('click', function () {
      event.preventDefault();
      var id = $(this).attr('href'),
        top = $(id).offset().top;
      $('body,html').animate({scrollTop: top}, 1500);
    });
  });
// menu side logic
  $('.navbar-menu-btn').on('click', function () {
    $('.navbar-nav, body, .navbar-menu-btn').toggleClass('active');
    $('.navbar-menu').fadeToggle(600);

    $('.header .nav-link').on('click', function () {
      $('.navbar-nav, body, .navbar-menu-btn').removeClass('active');
      $('.navbar-menu').fadeOut(600);
    });
  });
// set minimum page height
  function setMainHeight() {
    var mainH = $('main').height();
    var mainF = $(window).height() - $('header').height() - $('footer').height();
    $('main').css({
      height: (mainH >= mainF) ? mainH + 'px' : mainF + 'px'
    });
  }
// set height to card collections
  function setCardHeight() {
    var cardH = $('.card-block .imgLiquid').height();
    var cardF = $('.card-block .imgLiquid').width();
    $('.card-block .imgLiquid').css({
      height: (cardH < cardF) ? cardH + 'px' : cardF + 'px'
    });
  }
// on window resize resize blocks
  setCardHeight();
  $(window).resize(setCardHeight);
  setMainHeight();
  $(window).resize(setMainHeight);
// init waypoints js
  var waypoint1 = new Waypoint({
    element: document.getElementById('about'),
    handler: function () {
      $('#about').addClass('ani');
    },
    offset: '75%'
  });
  var waypoint2 = new Waypoint({
    element: document.getElementById('about_us'),
    handler: function () {
      $('#about_us .pic-wrap').each(function (i) {
        var that = $(this);
        that.find('.imgLiquid').css({"animation":"zoomIn 0.4s " + i * 0.3 + "s ease both", "opacity":"1"});
        that.find('.sub').css({"animation":"fadeInDown 0.3s " + i * 0.3 + "s ease both", "opacity":"1"});
      });
    },
    offset: '75%'
  });
  var waypoint3 = new Waypoint({
    element: document.getElementById('ecologist'),
    handler: function () {
      $('.eco-wrapper .title').addClass('ani');
      $('.eco-wrapper .text').addClass('ani');
    },
    offset: '50%'
  });
  var waypoint4 = new Waypoint({
    element: document.getElementById('colors'),
    handler: function () {
      $('.colors-wrapper .bg-wrap').addClass('ani');
      $('.colors-wrapper .text').addClass('ani');
    },
    offset: '50%'
  });
  var waypoint5 = new Waypoint({
    element: document.getElementById('services'),
    handler: function () {
      $('.services-wrapper .icons .icon').each(function (y) {
        var that = $(this);
        that.css({"animation":"zoomIn 0.4s " + y * 0.2 + "s ease both", "opacity":"1"});
      });
      $('.services-wrapper .text').addClass('ani');
    },
    offset: '60%'
  });
  var waypoint6 = new Waypoint({
    element: document.getElementById('delivery'),
    handler: function () {
      $('.delivery-wrapper .bg-wrap').addClass('ani');
      $('.delivery-wrapper .title').addClass('ani');
      $('.delivery-wrapper .text').addClass('ani');
    },
    offset: '75%'
  });
// init swiper js
  var swiper = new Swiper('.swiper-container', {
    pagination: '.swiper-pagination',
    paginationClickable: true,
    loop: true,
    autoplay: 4000
  });
// init phone input mask
  $("#inputPhone").mask("?+7 (999) 999-9999");
//init gallery js
  var initPhotoSwipeFromDOM = function (gallerySelector) {

    // parse slide data (url, title, size ...) from DOM elements
    // (children of gallerySelector)
    var parseThumbnailElements = function (el) {
      var thumbElements = el.childNodes,
        numNodes = thumbElements.length,
        items = [],
        figureEl,
        linkEl,
        size,
        item;

      for (var i = 0; i < numNodes; i++) {

        figureEl = thumbElements[i]; // <figure> element

        // include only element nodes
        if (figureEl.nodeType !== 1) {
          continue;
        }

        linkEl = figureEl.children[0]; // <a> element

        size = linkEl.getAttribute('data-size').split('x');

        // create slide object
        item = {
          src: linkEl.getAttribute('href'),
          w: parseInt(size[0], 10),
          h: parseInt(size[1], 10)
        };


        if (figureEl.children.length > 1) {
          // <figcaption> content
          item.title = figureEl.children[1].innerHTML;
        }

        if (linkEl.children.length > 0) {
          // <img> thumbnail element, retrieving thumbnail url
          item.msrc = linkEl.children[0].getAttribute('src');
        }

        item.el = figureEl; // save link to element for getThumbBoundsFn
        items.push(item);
      }

      return items;
    };

    // find nearest parent element
    var closest = function closest(el, fn) {
      return el && ( fn(el) ? el : closest(el.parentNode, fn) );
    };

    // triggers when user clicks on thumbnail
    var onThumbnailsClick = function (e) {
      e = e || window.event;
      e.preventDefault ? e.preventDefault() : e.returnValue = false;

      var eTarget = e.target || e.srcElement;

      // find root element of slide
      var clickedListItem = closest(eTarget, function (el) {
        return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
      });

      if (!clickedListItem) {
        return;
      }

      // find index of clicked item by looping through all child nodes
      // alternatively, you may define index via data- attribute
      var clickedGallery = clickedListItem.parentNode,
        childNodes = clickedListItem.parentNode.childNodes,
        numChildNodes = childNodes.length,
        nodeIndex = 0,
        index;

      for (var i = 0; i < numChildNodes; i++) {
        if (childNodes[i].nodeType !== 1) {
          continue;
        }

        if (childNodes[i] === clickedListItem) {
          index = nodeIndex;
          break;
        }
        nodeIndex++;
      }


      if (index >= 0) {
        // open PhotoSwipe if valid index found
        openPhotoSwipe(index, clickedGallery);
      }
      return false;
    };

    // parse picture index and gallery index from URL (#&pid=1&gid=2)
    var photoswipeParseHash = function () {
      var hash = window.location.hash.substring(1),
        params = {};

      if (hash.length < 5) {
        return params;
      }

      var vars = hash.split('&');
      for (var i = 0; i < vars.length; i++) {
        if (!vars[i]) {
          continue;
        }
        var pair = vars[i].split('=');
        if (pair.length < 2) {
          continue;
        }
        params[pair[0]] = pair[1];
      }

      if (params.gid) {
        params.gid = parseInt(params.gid, 10);
      }

      return params;
    };

    var openPhotoSwipe = function (index, galleryElement, disableAnimation, fromURL) {
      var pswpElement = document.querySelectorAll('.pswp')[0],
        gallery,
        options,
        items;

      items = parseThumbnailElements(galleryElement);

      // define options (if needed)
      options = {

        // define gallery index (for URL)
        galleryUID: galleryElement.getAttribute('data-pswp-uid'),

        getThumbBoundsFn: function (index) {
          // See Options -> getThumbBoundsFn section of documentation for more info
          var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
            pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
            rect = thumbnail.getBoundingClientRect();

          return {x: rect.left, y: rect.top + pageYScroll, w: rect.width};
        },

        shareEl: false,
        bgOpacity: 0.8

      };

      // PhotoSwipe opened from URL
      if (fromURL) {
        if (options.galleryPIDs) {
          // parse real index when custom PIDs are used
          // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
          for (var j = 0; j < items.length; j++) {
            if (items[j].pid == index) {
              options.index = j;
              break;
            }
          }
        } else {
          // in URL indexes start from 1
          options.index = parseInt(index, 10) - 1;
        }
      } else {
        options.index = parseInt(index, 10);
      }

      // exit if index not found
      if (isNaN(options.index)) {
        return;
      }

      if (disableAnimation) {
        options.showAnimationDuration = 0;
      }

      // Pass data to PhotoSwipe and initialize it
      gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
      gallery.init();
    };

    // loop through all gallery elements and bind events
    var galleryElements = document.querySelectorAll(gallerySelector);

    for (var i = 0, l = galleryElements.length; i < l; i++) {
      galleryElements[i].setAttribute('data-pswp-uid', i + 1);
      galleryElements[i].onclick = onThumbnailsClick;
    }

    // Parse URL and open gallery if it contains #&pid=3&gid=1
    var hashData = photoswipeParseHash();
    if (hashData.pid && hashData.gid) {
      openPhotoSwipe(hashData.pid, galleryElements[hashData.gid - 1], true, true);
    }
  };
  // execute above function
  initPhotoSwipeFromDOM('.gallery-wrap');
// init Yandex maps js
  ymaps.ready(init);
  function init() {
    var myMap = new ymaps.Map("map", {
        center: [56.30, 44.00],
        zoom: 13,
        controls: ['zoomControl']
      }),
      myGeoObject = new ymaps.GeoObject({
        geometry: {
          type: "Point",// тип геометрии - точка
          coordinates: [56.30, 44.00] // координаты точки
        }
      });
    myMap.geoObjects.add(myGeoObject);
  }

});
